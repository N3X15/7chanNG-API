swagger: '2.0'
info:
  version: "0.0.1-alpha"
  title: "7chan-NG API"
  license:
    name: MIT
produces:
  - application/json
  - application/x-yaml
host: '7chan.nexisonline.net'
parameters:
  format:
    description: "Request output format of API"
    name: format
    required: false
    in: query
    type: string
    enum:
      - yaml
      - yml
      - json
  schema:
    name: schema
    required: false
    type: boolean
    in: query
    description: 'Returns the JSON Schema of this API element.'
definitions:
  system_errors:
    name: system_errors
    type: array
    items:
      $ref: '#/definitions/system_error'
  system_error:
    title: Error Message
    type: object
    properties:
      name:
        type: string
        description: PHP constant name of the error.
        example: 'E_WARNING'
      file:
        type: string
        description: Path of the file that generated the error.
      line:
        type: integer
        description: Line in the file at which the error occurred.
      message:
        type: string
        description: The message of the error.
  status:
    type: boolean
    description: 'true if operation was successful.'
  message:
    type: string
    description: 'Contains any error message.'
  board:
    title: Board
    description: 'A board, represented as an object.'
    type: object
    properties:
      id:
        type: integer
        description: 'Uniquely-identifying ID of the board.'
      order:
        type: integer
        description: 'Used to tweak ordering manually.'
      name:
        type: string
        description: 'The name of the "directory" of the board (e.g. /b/).'
      type:
        type: string
        description: 'The type of board.'
      start:
        type: integer
        description: 'The beginning post ID.'
      uploadtype:
        type: string
        description: 'Unknown at this time.'
      desc:
        type: string
        description: 'The board description.'
      image:
        type: string
        description: 'Board header image URL'
      section:
        type: integer
        description: 'ID of the section the board is attached to.'
      maximagesize:
        type: integer
        description: 'Maximum size of uploaded files, in bytes.'
      maxpages:
        type: integer
        description: 'Maximum number of pages'
      maxage:
        type: integer
        description: 'Max age of threads (in days?)'
      markpage:
        type: integer
        description: 'The page on which threads are marked for deletion'
      maxreplies:
        type: integer
        description: 'Maximum number of replies before a thread is marked for deletion.'
      messagelength:
        type: integer
        description: 'Maximum length of a post'
      createdon:
        type: integer
        description: 'Time at which this board was created.'
      includeheader:
        type: string
        description: 'HTML to include in the board header.'
      redirecttothread:
        type: boolean
        description: 'Redirect back to the thread after posting.'
      anonymous:
        type: string
        description: 'Name of anonymous users.'
      embeds_allowed:
        type: boolean
        description: 'Are embeds allowed?'
      defaultstyle:
        type: string
        description: 'Default CSS stylesheet of the board.'
      locale:
        type: string
        description: 'HTML locale to use.'
      maxfiles:
        type: integer
        description: 'Maximum files per thread.'
      locked:
        type: boolean
        description: 'Board is locked and cannot be posted to.  Can be overridden by a site-wide lockdown mode.'
      forcedanon:
        type: boolean
        description: 'All posts are anonymous.'
      trial:
        type: boolean
        description: 'This board is being trialled.'
      popular:
        type: boolean
        description: 'This board is popular.'
      showid:
        type: boolean
        description: 'Show posters'' IDs.'
      compactlist:
        type: boolean
        description: Unknown
      enablereporting:
        type: boolean
        description: 'Enables reporting of posts.  Almost never false.'
      enablecaptcha:
        type: boolean
        description: 'Enables board captchas.  Can be overridden by a site-wide lockdown mode.'
      enablenofile:
        type: boolean
        description: 'Permits threads to be posted without files.'
      enablearchiving:
        type: boolean
        description: 'Allows posts to be archived.'
      enablecatalog:
        type: boolean
        description: 'Enables the catalog view'
      enablepostmoderation:
        type: boolean
        description: Unknown
      newsage:
        type: boolean
        description: Unknown

basePath: /api
schemes:
  - http
paths:
  "/board/{id}":
    get:
      description: |
        Get a `Board` object.
      parameters:
        - $ref: "#/parameters/format"
        - $ref: "#/parameters/schema"
        - name: id
          required: true
          in: path
          description: "`name` of the board."
          type: string
          format: '^[a-zA-Z_]$'
      responses:
        200:
          description: Successful response
          schema:
            title: 'Board View'
            description: 'A single board item.'
            type: object
            properties:
              status:
                $ref: '#/definitions/status'
              message:
                $ref: '#/definitions/message'
              system_errors:
                $ref: '#/definitions/system_errors'
              boards:
                "$ref": "#/definitions/board"
  "/boards":
    get:
      description: |
        Gets a list of `Boards` that are available to the public.
      parameters:
        - $ref: "#/parameters/format"
        - $ref: "#/parameters/schema"
      responses:
        200:
          description: Successful response
          schema:
            title: 'Board List'
            description: 'A list of publically-available boards and their properties.'
            type: object
            properties:
              status:
                $ref: '#/definitions/status'
              message:
                $ref: '#/definitions/message'
              system_errors:
                $ref: '#/definitions/system_errors'
              boards:
                type: array
                items:
                  "$ref": "#/definitions/board"
